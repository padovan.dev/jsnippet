package dev.padovan.xml.noschema;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import dev.padovan.xml.noschema.data.Head;

public class Unmarshall {

	public static void main(final String[] args) throws JAXBException {

		final ByteArrayOutputStream os = new ByteArrayOutputStream();
		Marshall.marshall(os);
		final InputStream is = new ByteArrayInputStream(os.toByteArray());
		final Head head = unmarshall(is);
		System.out.println(head.getDescription());

	}

	private static Head unmarshall(final InputStream is) throws JAXBException {
		final JAXBContext jaxbContext = JAXBContext.newInstance(Head.class);
		final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		final Head head = (Head) unmarshaller.unmarshal(is);
		return head;
	}
}

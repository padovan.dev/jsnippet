package dev.padovan.xml.noschema.data;

import java.math.BigDecimal;

public class Row {

	private boolean flag;
	private BigDecimal value;

	public BigDecimal getValue() {
		return this.value;
	}

	public void setValue(final BigDecimal value) {
		this.value = value;
	}

	public boolean isFlag() {
		return this.flag;
	}

	public void setFlag(final boolean flag) {
		this.flag = flag;
	}
}

package dev.padovan.xml.noschema.data;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Head {

	private int id;
	private String description;
	private List<Row> rows;

	public int getId() {
		return this.id;
	}

	public void setId(final int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public List<Row> getRows() {
		return this.rows;
	}

	public void setRows(final List<Row> rows) {
		this.rows = rows;
	}

}

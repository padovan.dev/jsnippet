package dev.padovan.xml.noschema;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import dev.padovan.xml.noschema.data.Head;
import dev.padovan.xml.noschema.data.Row;

public class Marshall {

	public static void main(final String[] args) throws JAXBException, IOException {

		try (OutputStream os = System.out) {
			marshall(os);
		}
	}

	public static void marshall(final OutputStream os) throws JAXBException {
		final Head head = getSample();

		final JAXBContext jaxbContext = JAXBContext.newInstance(Head.class);
		final Marshaller marshaller = jaxbContext.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		marshaller.marshal(head, os);
	}

	private static Head getSample() {
		final Head head = new Head();
		head.setId(1);
		head.setDescription("head description");

		final List<Row> rows = new ArrayList<>();
		final Row row1 = new Row();
		row1.setFlag(true);
		row1.setValue(BigDecimal.valueOf(Math.PI));
		rows.add(row1);
		final Row row2 = new Row();
		row2.setFlag(true);
		row2.setValue(null);
		rows.add(row2);
		head.setRows(rows);
		return head;
	}
}

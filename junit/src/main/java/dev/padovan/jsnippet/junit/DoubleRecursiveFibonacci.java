package dev.padovan.jsnippet.junit;

public class DoubleRecursiveFibonacci implements Fibonacci {

	@Override
	public long getFibonacci(final int index) {
		if (index < 0) {
			throw new IllegalArgumentException("Index should be 0 or positive");
		}
		return this.getFibonacci_(index);
	}

	private long getFibonacci_(final int index) {
		if (index == 0) {
			return 0;
		}
		if (index == 1) {
			return 1;
		}
		return this.getFibonacci_(index - 2) + this.getFibonacci_(index - 1);
	}

	@Override
	public boolean isFibonacci(final long value) {
		if (value < 0) {
			return false;
		}
		return this.isFibonacci(value, 0);
	}

	private boolean isFibonacci(final long value, final int i) {
		final long f = this.getFibonacci(i);
		if (f == value) {
			return true;
		}
		if (f < value) {
			return this.isFibonacci(value, i + 1);
		}
		return false;
	}
}

package dev.padovan.jsnippet.junit;

public class RecursiveFibonacci implements Fibonacci {

	@Override
	public long getFibonacci(final int index) {
		if (index < 0) {
			throw new IllegalArgumentException("Index should be 0 or positive");
		}
		return this.getFibonacci_(index)[1];
	}

	private long[] getFibonacci_(final int index) {
		if (index == 0) {
			return new long[] { 0, 0 };
		}
		if (index == 1) {
			return new long[] { 0, 1 };
		}
		final long[] fibonacci_1 = this.getFibonacci_(index - 1);
		return new long[] { fibonacci_1[1], fibonacci_1[0] + fibonacci_1[1] };
	}

	@Override
	public boolean isFibonacci(final long value) {
		if (value < 0) {
			return false;
		}
		return this.isFibonacci(value, 0);
	}

	private boolean isFibonacci(final long value, final int i) {
		final long f = this.getFibonacci(i);
		if (f == value) {
			return true;
		}
		if (f < value) {
			return this.isFibonacci(value, i + 1);
		}
		return false;
	}

}

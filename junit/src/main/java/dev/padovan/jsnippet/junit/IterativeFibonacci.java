package dev.padovan.jsnippet.junit;

public class IterativeFibonacci implements Fibonacci {

	@Override
	public long getFibonacci(final int index) {
		if (index < 0) {
			throw new IllegalArgumentException("Index should be 0 or positive");
		}
		if (index == 0) {
			return 0;
		}
		if (index == 1) {
			return 1;
		}
		long p1 = 0;
		long p2 = 1;
		for (int i = 2; i <= index; i++) {
			final long p3 = p1 + p2;
			p1 = p2;
			p2 = p3;
		}
		return p2;
	}

	@Override
	public boolean isFibonacci(final long value) {
		if (value < 0) {
			return false;
		}
		if (value <= 1) {
			return true;
		}
		long p1 = 0;
		long p2 = 1;
		while (p2 < value) {
			final long p3 = p1 + p2;
			if (p3 == value) {
				return true;
			}
			p1 = p2;
			p2 = p3;
		}
		return false;
	}
}

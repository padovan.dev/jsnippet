package dev.padovan.jsnippet.junit;

public interface Fibonacci {

	long getFibonacci(int index);

	boolean isFibonacci(long value);
}

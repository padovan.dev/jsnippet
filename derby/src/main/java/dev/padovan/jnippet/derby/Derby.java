package dev.padovan.jnippet.derby;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;

public class Derby {

	public static void main(final String[] args) throws SQLException {

		final String dbPath = args.length == 0 ? "data" : args[0];

		try (final Connection connection = DriverManager.getConnection("jdbc:derby:" + dbPath + ";create=true")) {
			try (Statement statement = connection.createStatement()) {

				try (ResultSet tables = connection.getMetaData().getTables(null, "APP", "TBL", null)) {
					if (!tables.next()) {
						statement.execute("create table TBL(id int primary key, value varchar(255))");
					}
				}

				try (PreparedStatement ps = connection.prepareStatement("insert into tbl values(?,?)")) {
					for (int i = 0; i < 5; i++) {
						final int id = new Random().nextInt();
						ps.setInt(1, id);
						ps.setString(2, "value:" + id);
						ps.execute();
					}
				}
				try (ResultSet rs = statement.executeQuery("select * from tbl")) {
					while (rs.next()) {
						System.out.println(rs.getInt(1) + " - " + rs.getString(2));
					}
				}
			}
		}
	}
}
